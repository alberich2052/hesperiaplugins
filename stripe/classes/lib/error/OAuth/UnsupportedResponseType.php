<?php

namespace HesperiaPlugins\Stripe\Classes\lib\Error\OAuth;

/**
 * UnsupportedResponseType is raised when an unsupported response type
 * parameter is specified.
 */
class UnsupportedResponseType extends OAuthBase
{
}
