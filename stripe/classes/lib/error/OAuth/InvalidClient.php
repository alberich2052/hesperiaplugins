<?php

namespace HesperiaPlugins\Stripe\Classes\lib\Error\OAuth;

/**
 * InvalidClient is raised when authentication fails.
 */
class InvalidClient extends OAuthBase
{
}
