<?php

Route::get('Hesperia-Eden-Club', function () {
    return Redirect::to("hotel/hesperia-eden-club");
});

Route::get('Hesperia-WTC-Valencia', function () {
    return Redirect::to("hotel/hesperia-wtc-valencia");
});

Route::get('Hesperia-Playa-el-Agua', function () {
    return Redirect::to("hotel/hesperia-playa-el-agua");
});

Route::get('Hesperia-Isla-Margarita', function () {
    return Redirect::to("hotel/hesperia-isla-margarita");
});

Route::get('Hesperia-Maracay', function () {
    return Redirect::to("hotel/hesperia-maracay");
});

 ?>
