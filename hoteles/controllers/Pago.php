<?php namespace HesperiaPlugins\Hoteles\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Pago extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController','Backend\Behaviors\ReorderController'];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('HesperiaPlugins.Hoteles', 'main-menu-item2', 'side-menu-item2');
    }

    public function relationExtendManageWidget($widget, $field, $model){
        trace_log($field);
        // Make sure the field is the expected one
        if ($field != 'myField')
            return; 
    
        // manipulate widget as needed
        }
}